## INTRODUCTION
 
 Provides an extra content info that displays the time it will take to read 
 a content on your site.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/ert

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/ert


## INSTALLATION

Install as usual, see
 https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
information.